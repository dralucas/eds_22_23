# LAB 1 Inspect your data and Sensor calibration


This lab contains three steps.

`Lab_1_0` :: Positionning with numpy

`Lab_1_1` :: Inspecting your data

`Lab_1_2` :: Calibrating a sensor with data sync, using regression, ransac and PCA


