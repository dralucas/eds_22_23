#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 15:09:57 2021

@authors: Grégory Sainton & Antoine Lucas
@purpose: Lib linked to the Lab3 notebook EDS_2020_2021_3D_LiDAR_Claiff_ML_*.ipynb
			It contains useful function to run the lab, to plot several sets of data
			

"""

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import numpy as np


def readData(filename):
	"""
		Function to read the landscape data
		INPUT:
		------
			@filename: string

		OUTPUT:
		-------
			x, y, z: numpy arrays

	""" 
	import numpy as np
	import os
	
	if os.path.isfile(filename):
		f = open(filename)
		floor = np.genfromtxt(filename)
		x = floor[:,0]  
		y = floor[:,1]
		z = floor[:,2]
		f.close()
		
		return x, y, z
		
	else:
		print(f'File {filename} is not accessible')




def plot_figs(fig_num, elev, azim, dx, dy, dz, density=2):
	"""
	Draw (x, y z) coordinates into a 3 dimensional scatter plot.
	INPUT:
	------
		@fig_num: int   -  Figure number
		@elev   : float -  Elevation 
		@azim   : float -  Azimuth
		@dx, @dy, dz : numpy arrays - data to plot
		@density: int - optionnal - factor to decimate the data on plot
	OUTPUT:
	-------
		None 
	
	"""

	
	# Subsample input data with a factor "density"
	X = dx[::density]
	Y = dy[::density]
	Z = dz[::density]
	
	# Plot the data in 3D
	fig = plt.figure(figsize=(10, 8))
	plt.clf()
	ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=elev, azim=azim)
	ax.scatter(dx[::density], dy[::density], dz[::density], c=dz[::density], marker='+', alpha=.4)
	
	# OPTIONNAL - BUT NICE TO IMPROVE YOUR POINT OF VIEW
	# Create a blanck cubic bounding box to simulate equal aspect ratio in 3D plots
	max_range = np.array([X.max()-X.min(), Y.max()-Y.min(), Z.max()-Z.min()]).max()
	Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(X.max()+X.min())
	Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(Y.max()+Y.min())
	Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(Z.max()+Z.min())
	for xb, yb, zb in zip(Xb, Yb, Zb):
		ax.plot([xb], [yb], [zb], 'w')
	plt.show()




def getEigenvaluePCA(a, b, c, dim, decim=2):
	"""
	Function to estimate the eigenvalues of the PCA 
	for every points into a given sphere of radius dim/2
	------
	INPUT:
		@a, @b, @c: numpy arrays with the coordinates
		@dim	  : float - diameter of interest of the neighborhood ball
		@decim	: integer - decimation factor to lower the time processing.
	-----
	OUTPUT:
		@comp	 : array of array with the 3 eigenvalues
	
	EXAMPLE: 
		[[0.68693784 0.31106007 0.0020021 ]
		 [0.75319626 0.24592732 0.00087642]
		 [0.78309201 0.21595601 0.00095197]
		 ...]
	
	"""
	from sklearn.decomposition import PCA
	#from scipy import spatial
	
	
	
	print('Data are decimed by a factor of ', decim)
	# Remove the mean of each vector
	a -= np.mean(a, axis=0)
	b -= np.mean(b, axis=0)
	c -= np.mean(c, axis=0)	
	# Concat the coordinates arrays into a single matrix.
	Y = np.c_[a[::decim], b[::decim], c[::decim]]
	print("shape of Y " ,np.shape(Y))

	# Estimate the distance of one point with every neighbour around
	#dist = spatial.distance.squareform(spatial.distance.pdist(Y))	
	

	
	n_samples = Y.shape[0]		   # number of points in Y
	
	print('  Treating distance of', dim, 'm')
	comp = []						# Initialize the eigenvalues components array

	# Loop over every points to have a look on its neighbour.
	#   1. We consider a sphere of radius dim/2 and we only consider 
	#				the neighbours inside this sphere
	#   2. We create a subsample with these points
	#   3. Using PCA scikit-learn function, we estimate the 3 eigenvalues of these points
	#   4. We concatenate these 3 eigenvalues in a single array which will be return at the end.
	for kk in range(0,n_samples):
		dist =  np.sqrt((Y[kk,0] - Y[:,0])**2 + (Y[kk,1] - Y[:,1])**2 + (Y[kk,2] - Y[:,2])**2)

		# Mask of values corresp. to the criteria
		pts = np.where((dist<=dim/2))
									 

		if np.size(pts)<3:			 # Check if the number of neighbour is sufficient	 
			#print("Revise dims: ")
			#print(pts)
			#print(dist)
			eigenvalues=np.array([0., 0., 0.])

		else:
			Ytmp = Y[pts,:]			 # Apply the criteria on the dataset
			Ytmp = Ytmp[0,:,:]		  # Reduce the depth of the array	   
										# (not necessary depending on the data structure used)
										# Equivalent to Ytmp = np.squeeze(Ytmp)

			pca = PCA(n_components=3)   # Instantiate the PCA object 
			pca.fit(Ytmp)
			eigenvalues = pca.explained_variance_ratio_
			#sval = pca.singular_values_
			#print('------')
			#print( eigenvalues) 
			#print( sval**2 / np.sum(sval**2) )
			comp.append(eigenvalues)	# Append the 3 eigenvalues to "comp" array
	comp = np.array(comp)
	return comp




def estimateTernaryCoord(comp):
	"""
	Function which estimate the ternary coordinates 
	from the list of eigenvalues.
	----
	INPUT:
		@comp: np array - list of eigenvalue for every 
				points with a given dimension
	OUTPUT:
	-------
		@X, @Y, @pdf
	
	"""
	# Conversion towards a,b,c space  
	# 
	from scipy.stats import gaussian_kde
	
	P1 = comp[:,0]
	P2 = comp[:,1]
	P3 = comp[:,2]
	
	
	# 2021-01/25 - Bug fixed in the calculation of xp1p2
	# Consider a point of coordinates (p1, p2) -> Red point on the graphic above.
	# What is the projection of this point on the line (p1p2) ? 
	# We know that p1+p2+p3 = 1 -> p3 = 1-p1-p2
	# To find the projection x_p1p2 : x_p1p2 = p1 + dx 
	#		  with dx	= p3.cos(theta)
	#		  with theta = angle of the slope (-pi/4)
	# It comes x_p1p1 = p1 + (1-p1-p2) * cos(theta)	 
	xp1p2 = P1 + (1-P2-P1)*np.cos(np.pi/4)	
	
	# yp1p2 is derive from the fact that the slope is -1.
	yp1p2 = 1 - xp1p2							
	
	# Estimate distances along p1-p2 axis
	SmallDelta   = np.sqrt((.5 -  xp1p2)**2 + (.5 - yp1p2)**2)
	BigDelta	 = np.sqrt((0.5 -  1)**2 + (0.5 - 0)**2)
	
				
	# Estimate a,b and c coordinates from the results just above			
	c = 3 * P3
	a = (1 - c)*SmallDelta/BigDelta
	b = 1 - (c + a) 
	
	
	# Norm factor: the sum of the eigenvalue (p1, p2 and p3 is equal to 1)
	v = (a + b + c)					

	# Conversion towards ternary graph 
	X = .5 * ( 2.*b+c ) / v		
	Y = 0.5*np.sqrt(3) * c / v

	# Compute density probability function for clarity
	# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.gaussian_kde.html
	xy = np.vstack([X, Y])
	pdf = gaussian_kde(xy)(xy)
	
	return X, Y, pdf 






def plotTernary(x, y, z, title):   
	"""
	Function to plot a ternary graph from list of ternary coordinates.
	----
	INPUT:
		@x, @y, @z: numpy arrays - ternary coordinates
		@title : string - title of the graph
	----
	OUTPUT:
		None
	
	"""
	import matplotlib.tri as tri
	
	
	# Plot part
	plt.figure(figsize=(7, 5))
	plt.clf()
	plt.scatter(x,y,c=z, s= 0.5)	 

	# creating the TRI grid
	corners = np.array([[0, 0], [1, 0], [0.5,  np.sqrt(3)*0.5]])
	triangle = tri.Triangulation(corners[:, 0], corners[:, 1])
	refiner = tri.UniformTriRefiner(triangle)
	trimesh = refiner.refine_triangulation(subdiv=4)

	# plotting the mesh
	plt.triplot(trimesh,'k-', linewidth=0.1)
	plt.title(title)
	
	plt.axis('equal')
	plt.axis('off')

	plt.show()
   












def plot_contours_compare(X, y, X_train , y_train, X_test, y_test, classifiers, plot_input = False):
	"""
		Function to plot in the same plot several classifiers 
		contour to compare them.
		The librairies of the classifiers are supposed to be loaded out of the functions
		since we don't know a priori the type of classifiers in input.
		----
		INPUT:
			@X: DataFrame of data
			@y: DataFrame of label
			@X_train: Training set of data
			@y_train: Training set of label
			@X_test: Test set of data
			@y_test: Test set of label
			@classifiers: list - list of instance of classifiers
						ie : classifiers = [LinearDiscriminantAnalysis(store_covariance=True),
											DecisionTreeClassifier()]
			plot_input: boolean - optionnal - Option to plot the input 
									data alone in a separate plot
		----
		OUTPUT
			None
			
		EXAMPLE: 
		-------
		 classifiers = [
			LinearDiscriminantAnalysis(store_covariance=True),
			SVC(kernel="linear", C=0.025),
			QuadraticDiscriminantAnalysis(), 
			DecisionTreeClassifier(), 
			RandomForestClassifier()]	
		plot_contours_compare(X, y, X_train , y_train, X_test, y_test, classifiers, plot_input = False)
			
	"""
	# Code adapted from  
	# https://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html

	import numpy as np
	import matplotlib.pyplot as plt
	from matplotlib.colors import ListedColormap
	
	from matplotlib import colors

	cmap = colors.LinearSegmentedColormap(
		'red_blue_classes',
		{'red': [(0, 1, 1), (1, 0.7, 0.7)],
		 'green': [(0, 0.7, 0.7), (1, 0.7, 0.7)],
		 'blue': [(0, 0.7, 0.7), (1, 1, 1)]})

	plt.cm.register_cmap(cmap=cmap)
	
	
	nb_plot=len(classifiers)+1 if plot_input else len(classifiers)
	
	# Here is defined the list of classifier's name
	names = ["LDA","Linear SVM", "QDA", "Decision Tree", "Random Forest"]

	
	figure = plt.figure(figsize=(27, 9))
	i = 1
	
	h = .02  # step size in the mesh
	x_min, x_max = X.iloc[:, 0].min() - .5, X.iloc[:, 0].max() + .5
	y_min, y_max = X.iloc[:, 1].min() - .5, X.iloc[:, 1].max() + .5
	xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

	ds_cnt = 0
	
	# just plot the dataset first
	cm = plt.cm.RdBu
	#cm_bright = ListedColormap(['#FF0000', '#0000FF'])
	cm_bright = ListedColormap(['#FF0000', '#0000FF'])
	ax = plt.subplot(1, len(classifiers) + 1, i)
	if ds_cnt == 0:
		ax.set_title("Input data")
	# Plot the training points
	ax.scatter(X_train.iloc[:, 0], X_train.iloc[:, 1], c=y_train, cmap=cm_bright,
			   edgecolors='k')
	# Plot the testing points
	ax.scatter(X_test.iloc[:, 0], X_test.iloc[:, 1], c=y_test, cmap=cm_bright, alpha=0.6,
			   edgecolors='k')
	ax.set_xlim(xx.min(), xx.max())
	ax.set_ylim(yy.min(), yy.max())
	ax.set_xticks(())
	ax.set_yticks(())

	i+=1
	ds_cnt+=1
	# iterate over classifiers
	for name, clf in zip(names, classifiers):
		ax = plt.subplot(1, len(classifiers) + 1, i)
		clf.fit(X_train, y_train)
		score = clf.score(X_test, y_test)

		# Plot the decision boundary. For that, we will assign a color to each
		# point in the mesh [x_min, x_max]x[y_min, y_max].
		if hasattr(clf, "decision_function"):
			Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
		else:
			Z = clf.predict_proba(-np.c_[xx.ravel(), yy.ravel()])[:, 1]

		# Put the result into a color plot
		Z = Z.reshape(xx.shape)
		#ax.contourf(xx, yy, Z, cmap=cm, alpha=.8)
		
		ax.pcolormesh(xx, yy, Z, cmap='red_blue_classes',
				   norm=colors.Normalize(0., 1.), zorder=0,shading='auto')
		ax.contour(xx, yy, Z, [0.5], linewidths=2., colors='white')
		
		#ax.contourf(xx, yy, Z, cmap=cm, alpha=.8)
		#'red_blue_classes'

		# Plot the training points
		ax.scatter(X_train.iloc[:, 0], X_train.iloc[:, 1], c=y_train, cmap=cm_bright,
				   edgecolors='k')
		# Plot the testing points
		ax.scatter(X_test.iloc[:, 0], X_test.iloc[:, 1], c=y_test, cmap=cm_bright,
				   edgecolors='k', alpha=0.6)

		ax.set_xlim(xx.min(), xx.max())
		ax.set_ylim(yy.min(), yy.max())
		ax.set_xticks(())
		ax.set_yticks(())
		ax.set_title(name)
		ax.text(xx.max() - .3, yy.min() + .3, ('%.2f' % score).lstrip('0'),
				size=15, horizontalalignment='right')
		i += 1

	plt.tight_layout()
	plt.show()


###


def plot_contours(X, y, classifier, resolution=0.02,level=50):
	"""
	Function to plot single classifier contours
	----
	INPUT:
		@X: Numpy array with data, e.g. X.to_numpy()
		@y: Numpy array with labels
		@resolution: resolution of the contour plot
		@level: depth of the intersection of hyperplanes
		
	----
	OUTPUT:
		None
	----
	EXAMPLE: 
		
		plot_contours(X_train.to_numpy(), y_train.to_numpy(), lda)
		plt.legend(loc='upper left')
		plt.tight_layout()
		plt.show()
	"""

	import numpy as np
	import matplotlib.pyplot as plt
	from matplotlib.colors import ListedColormap
	
	from matplotlib import colors

	# setup marker generator and color map
	# markers = ('s', 'x', 'o', '^', 'v')
	# colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
	# cmap = ListedColormap(colors[:len(np.unique(y))])
	# C = X[:, 0:2]

    # # plot the decision surface
	# x1_min, x1_max = C[:, 0].min() - 1, C[:, 0].max() + 1
	# x2_min, x2_max = C[:, 1].min() - 1, C[:, 1].max() + 1
	# xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
	# 					   np.arange(x2_min, x2_max, resolution))
	# Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
	# Z = Z.reshape(xx1.shape)
	# plt.contourf(xx1, xx2, Z, alpha=0.4, cmap=cmap)
	# plt.xlim(xx1.min(), xx1.max())
	# plt.ylim(xx2.min(), xx2.max())

	# for idx, cl in enumerate(np.unique(y)):
	# 	plt.scatter(x=C[y == cl, 0], y=C[y == cl, 1],
	# 				alpha=0.8, c=cmap(idx),
	# 				marker=markers[idx], label=cl)

	x_values = np.linspace(0, 1, np.int32(1/resolution))
	x_mesh = np.meshgrid(x_values, x_values)
	x_plane = np.array([x.ravel() for x in x_mesh])
	x_projection = level * X[:, 3].mean()   * x_plane
	x_input = np.vstack((x_plane, x_projection))
	y_boundary = classifier.predict(x_input.T)
	plt.figure()
	plt.contourf(
    	x_mesh[0],
    	x_mesh[1],
    	y_boundary.reshape(x_mesh[0].shape),
    	levels=[0.5, 1.5, 2.5],
    	zorder=0,
		)
	plt.scatter(
    	X[:, 0],
    	X[:, 1],
    	c=y,
    	cmap="tab20",
    	zorder=2,
	)		


def plot_3dcladd(dx,dy,dz,y,density=1):
    X = dx[::density]
    Y = dy[::density]
    Z = dz[::density]
    elev = 36
    azim = -144
    # Plot the data in 3D
    fig = plt.figure(figsize=(10, 8))
    plt.clf()
    ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=elev, azim=azim)
    ax.scatter(dx[::density], dy[::density], dz[::density], c=y, marker='.', alpha=.5,cmap='YlGn')
    
    # OPTIONNAL - BUT NICE TO IMPROVE YOUR POINT OF VIEW
    # Create a blanck cubic bounding box to simulate equal aspect ratio in 3D plots
    max_range = np.array([X.max()-X.min(), Y.max()-Y.min(), Z.max()-Z.min()]).max()
    Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(X.max()+X.min())
    Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(Y.max()+Y.min())
    Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(Z.max()+Z.min())
    for xb, yb, zb in zip(Xb, Yb, Zb):
       ax.plot([xb], [yb], [zb], 'w')
    plt.show()


def plot_confMat(cnf_matrix,ClaName):
	"""
	Usefull function to plot a confusion matrix
	----
	INPUT:
		cnf_matrix: confusion matrix calculated with scikit
		ClaName: string : name of the classifier
	
	"""
	import pandas as pd
	import seaborn as sns
	import numpy as np
	import matplotlib.pyplot as plt

	class_names=[0,1,2,3] # name  of classes
	fig, ax = plt.subplots()
	tick_marks = np.arange(len(class_names))
	plt.xticks(tick_marks, class_names)
	plt.yticks(tick_marks, class_names)
	# create heatmap
	sns.heatmap(pd.DataFrame(cnf_matrix), annot=True, cmap="YlGnBu" ,fmt='g')
	ax.xaxis.set_label_position("top")
	plt.tight_layout();
	plt.title("Confusion matrix using "+ClaName, y=1.1);
	plt.ylabel('Actual label');
	plt.xlabel('Predicted label');
	plt.show()