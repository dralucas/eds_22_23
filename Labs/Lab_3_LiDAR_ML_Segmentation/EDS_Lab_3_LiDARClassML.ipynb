{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Lab3: LiDAR 3D Cloud classification using Machine Learning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Prerequisites\n",
    "\n",
    "$\\color{red}{Please\\ rename\\ your\\ Notebook\\ before\\ starting\\ to\\ avoid\\ push/pull\\ issue\\ while\\ loading\\ the\\ corrections.}$\n",
    "\n",
    "\n",
    "\n",
    "**Author**: Antoine Lucas (lucas@ipgp.fr) / Grégory Sainton (sainton@ipgp.fr)\n",
    "\n",
    "**Update**: \n",
    "- v0.1: Oct 2019 (init. version)\n",
    "- v1.0: Jan 2020 (Notebook version)\n",
    "- V1.1: Jan 2021, 15th (Corrections and update)\n",
    "- V1.2: Jan 2021, 25th: fix typo, ternary coordinates errors\n",
    "- v2.0: Jan 2022, Make use of the external lib for the lab\n",
    "\n",
    "\n",
    "**Purpose** : We aim at performing a classification over a 3D cloud obtained from ground LiDAR. The technique is based on Brodu & Lague, (2012) paper [1]. The pdf of the paper is available on this GitLab. \n",
    "Assuming some considerations about the 3D distribution of objects at various scales, we will compute the eigenvalues (out of PCA) for some training set. Hence we will apply some reference frame transformations, and finally train a classifier. The ultimate goal is to be able to classify a whole data set.\n",
    "\n",
    "**Data**:\n",
    "- There are three data sets but for now let's only consider data sets #1: \n",
    "    1. ``LiDARDunes`` containing a whole vegetated dunes scene with 2 files for training in the `/data/EDS/Lab_3/LiDARDunes/training/`, `floor.xyz` and `vegetation.xyz`\n",
    "\n",
    "**Warning** This time, the data are already installed on the system. You will find the data sets in `/data/EDS/Lab_3/`. \n",
    "\n",
    "\n",
    "**/!\\** We will build first our workflow using the ``LiDARDunes``, and applied it on others if you go fast. \n",
    "\n",
    "Read the reference paper [1] for additionnal information about the ``LiDARDunes`` data\n",
    "\n",
    "\n",
    "\n",
    "**Best Practices**\n",
    "Since Python is now like a second langague, we are going a bit further in the coding best practice. \n",
    "1. As you've seen in the previous labs, everything can be coded into a single Notebooks. But in order to improve the readability of the notebook, it' often better to write most of the function into a script file (.py file) and call the library of functions in your notebook.\n",
    "2. Every function should have a `*docstring*` ie. a documentation describing its purpose, its input and output parameters.\n",
    "3. Follow as much as possible the `PEP8` convention in coding style.\n",
    "4. Don't hesitate to catch possible errors (not existing files, empty arrays...) with `try`statement or any conditionnal statement.\n",
    "\n",
    "**You will learn**:\n",
    "- How to use PCA to reduce dimensionality;\n",
    "- How to build ternary plots at different scales;\n",
    "- Split your data into a training set and a test set;\n",
    "- How to handle several classification algorithms.\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "### Organization of the notebook\n",
    "1. Preprocessing of the data\n",
    "\n",
    "    1.1. Write a function to read the data\n",
    "    \n",
    "    1.2. Write a function to plot those data\n",
    "    \n",
    "    \n",
    "2. Local dimensionality at a given scale\n",
    "\n",
    "    2.1. PCA computation at each points\n",
    "    \n",
    "    2.2. Calculate the ternary coordinates\n",
    "    \n",
    "    2.3. Plot the ternary graph \n",
    "    \n",
    "    2.4. Plot the graph for different scales\n",
    "    \n",
    "    2.5. Prepare the second training set (i.e., vegetation)\n",
    "    \n",
    "    2.6. Concatenate both training sets (vegetation and floor)\n",
    "    \n",
    "    \n",
    "3. Machine Learning classification\n",
    "\n",
    "    3.1. Now, let's prepare the results of the PCA for ML classification\n",
    "    \n",
    "    3.2. Apply classifiers on the data\n",
    "    \n",
    "    \n",
    "4. From the prototype, we will create a PEP-8 python script for Big Data application\n",
    "    \n",
    "    4.1. Copy the code section into a single script\n",
    "    \n",
    "    4.2. Export as external lib all functions\n",
    "    \n",
    "    4.3. Speed-up some steps\n",
    "    \n",
    "    4.4. Execute the new worflow on another data set based on the trained algo from initial data set\n",
    "    \n",
    "    4.5. Discuss the results\n",
    "    \n",
    "    \n",
    "----------------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Before going further, execute the following cell to import all the necessary libraries for the lab."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from numpy import savetxt\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Internal Library associated to this lab.\n",
    "#\n",
    "# Please note that if you make any modification in this library, \n",
    "# you need to restart the Kernal of your notebook to take the \n",
    "# changes into account (Kernel -> Restart or Restart and Clear Output)\n",
    "from libLab3_Lidar import *\n",
    "%matplotlib widget"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Preprocessing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1 Look at the function `readData` in the Lib file to read the data\n",
    "The files are made with 3 columns.\n",
    "The readin function follows the following model: \n",
    "```python\n",
    "    def readData(filename):\n",
    "        \"\"\"\n",
    "        Function to read the landscape data\n",
    "        INPUT:\n",
    "        ------\n",
    "            @filename: string\n",
    "        \n",
    "        OUTPUT:\n",
    "        -------\n",
    "            x, y, z: numpy arrays\n",
    "        \n",
    "        \"\"\"\n",
    "```\n",
    "\n",
    "**hint**\n",
    "- Here we used ``np.genfromtxt(filename, delimiter=' ')`` to read the *.xyz files\n",
    "- Since you return three objects, you need to use three objects while calling the function:\n",
    "```python\n",
    "    dx1, dy1, dz1 = readData(filename)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Open one file with the  `readData`  function. Again, the data are located at `/data/EDS/Lab_3/`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2. Plot function\n",
    "- to plot your data into (x, y, z) in a 3D scatter plot\n",
    "    \n",
    "```python\n",
    "    def plot_figs(fig_num, elev, azim, dx, dy, dz, density=2):\n",
    "\n",
    "```\n",
    "\n",
    "- One can specify the figure number (`fig_num`),  the `elevation` and the `azimuth` parameters to change the point of view of your plot.\n",
    "- `density` option will be your decimation factor.\n",
    "- One easy way to decimate a numpy array with a decimation factor `d` is the following:\n",
    "```python\n",
    "    myarray_decim = myarray[::d]\n",
    "```\n",
    "\n",
    "**hints**\n",
    "- For your 3D plot, we consider using `Axes3D` from the`mpl_toolkits.mplot3d` library.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot your dataset with the `plot_figs` function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Local dimensionality at a given scale"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As written in the article, the main idea is to characterize the local dimensionality properties for **each point** at **different scales** $s$.\n",
    "Let's consider $s$ as the diameter of a ball centered on a given point of interest. One can compute a neighborhood ball any scale of interest for each point of the scene. \n",
    "\n",
    "To find the most relevant directions in the neighborhood ball, we are using PCA to maximize the variance between the neighbors (see Lab1 for details), \n",
    "\n",
    "\n",
    "<img src=\"./Figures/NeighboorBall.png\" width=\"800\">\n",
    "\n",
    "So, in short, the next step is to estimate the PCA for each point $(X,Y,Z)$, center of a neighborhood ball with a diamater $s$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1 PCA computation at each points\n",
    "#### Questions\n",
    "\n",
    "- Create a function that estimate the PCA and returns the eigenvalues of every points within a scale distance \"s\"\n",
    "\n",
    "**hints**\n",
    "- Anticipate in your function the fact that you will have to decimate your data\n",
    "- Center your vectors X, Y, Z by removing the mean value (`np.mean(...)`)\n",
    "- With the commands `spatial.distance.pdist` and `spatial.distance.squareform` compute all the distances from one point to the others.\n",
    "- For each point\n",
    "    - Select all the points within the diameter $s$ \n",
    "    - Estimate the PCA from explained variance ratio\n",
    "    - Keep the eigenvalues in an array\n",
    "    \n",
    "- PCA documentation is here https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html?highlight=pca#sklearn.decomposition.PCA\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Prototype your code here, then export it into the external libLab3_Lidar lib"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Test on the first values. \n",
    "Never hesitate to add some extra cells to get some partial results\n",
    "Here, before going further, it's better to have a look on the output structure of the function you just wrote.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE YOUR CODE HERE "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 Calculate the ternary coordinates"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the following formula, we are able to transform the eigenvalues to plot them into a ternary graph.\n",
    "\n",
    "<center><img src = \"./Figures/2D_ParameterSpace_Ternary.png\" witdh = \"300\"></center>\n",
    "\n",
    "\n",
    "#### Questions\n",
    "- Write a function `estimateTernaryCoord` which estimate the ternary component for each points. For this, you will need to compute the small delta and other variables as stated in the figure.\n",
    "- The function will return the list of coordinates\n",
    "\n",
    "```python\n",
    "    def estimateTernaryCoord(comp):\n",
    "        ...\n",
    "        return X, Y, pdf\n",
    "\n",
    "```\n",
    "\n",
    "x_ter and y_ter being from the figure above.\n",
    "z_ter, being the density obtained from \n",
    "\n",
    "- `np.vstack([X, Y])` and \n",
    "- the scipy function `gaussian_kde`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Prototype your code here, then export it into the external libLab3_Lidar lib"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3. Plot the ternary graph\n",
    "\n",
    "Using the coordinates estimated in the previous question, plot the ternary graph from `plotTernary` function of the lib.\n",
    "```python\n",
    "   def plotTernary(x, y, z, title):  \n",
    "        ...\n",
    "```\n",
    "**hints**: `tri.Triangulation`, `tri.UniformTriRefiner`, `.refine_triangulation` and `plt.triplot` have been used in the function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use HERE the plotTernary function to plot the PCA feature extraction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.4. Plot the graph for different scales\n",
    "\n",
    "#### Questions\n",
    "Since all the pieces of the puzzle are done: \n",
    "\n",
    "- Plot the ternary graph for several scales for example $10$ cm and $30$ cm with a decimation factor equal to $4$;\n",
    "- Create a Pandas DataFrame `floor` with the 3 ternary components for each dimensions. The coordinates of both dimensions must be concatenated horizontally using `pd.concat([..., ...], axis=1)`;\n",
    "- Add a column full of $1$ to identify the data from the first file. This column will be used as a label column.\n",
    "\n",
    "\n",
    "**hint**\n",
    "\n",
    "Here is the header of the expected DataFrame `floor` after concatenation and after adding the field `class`:\n",
    "\n",
    "|x_ter_0.1|y_ter_0.1|z_ter_0.1|x_ter_0.3|y_ter_0.3|z_ter_0.3|class|\n",
    "|---------|---------|---------|---------|---------|---------|-----|\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Example of `head` for the DataFrame `floor'\n",
    "\n",
    "<img src=\"./Figures/floorDataFrame.png\" width = \"400\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Comments\n",
    "Interestingly, the center of mass is clearly close to a pure 2D distribution. \n",
    "But when looking at 10cm, we detect the ripples, while at 30cm the dune (and it's avalanche face) is detectable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.5 Prepare the second training set (i.e., vegetation)\n",
    "\n",
    "In order to prepare our further classifiers, we need another dataset representing another type of data. \n",
    "\n",
    "In the directory `./LiDARDunes/Training/`, there is a file called `vegetation.xyz`.\n",
    "\n",
    "#### Questions\n",
    "1. Open the file just like you did with `floor.xyz`\n",
    "2. Apply the steps 2.1 to 2.4 to your second file. \n",
    "3. The DataFrame will be named `veget` and the column `class` will be full of $2$. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Comments\n",
    "Interestingly, the center of mass is moving from a 3D distribution towards a more ellongated shape from 10 cm to 30 cm. \n",
    "This is due to the whole shape of the vegetation, which is ellongated at a scale greater than 20cm."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.6. Concatenate both training sets\n",
    "\n",
    "#### Question\n",
    "\n",
    "Using the function `pd.concat(...)`, concatenate the two DataFrame `floor` and `veget`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE YOUR CODE HERE\n",
    "data = pd.DataFrame()\n",
    "# Go on..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Machine Learning classification\n",
    "\n",
    "In this section, we will use all the preprocessing done in the section 1. and 2. to compare several classification algorithms on the data. \n",
    "\n",
    "\n",
    "\n",
    "You will explore several functions of the `sci-kit learn` library. Don't hesitate to have a look on https://scikit-learn.org/stable/index.html (dedicated web site) to find informations about the functions, about the parameters and espacially the[https://scikit-learn.org/stable/supervised_learning.html#supervised-learning](classification part)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.1 Now, let's prepare the results of the PCA for ML classification\n",
    "\n",
    "1. Split the dataframe into X,y variables, X being the features, y being the class. \n",
    "    You can use ``pd.drop()`` for the X so to drop some unwanted fields ``('z_ter_0.1','z_ter_0.3','class')``  for the learning stage.\n",
    "    \n",
    "2. Split this data sets into training and testing subsets with ``train_test_split`` command. We suggest a ``test_size=.25``."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Before going further, execute the following cell to import all the necessary libraries for the lab."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.tree import DecisionTreeClassifier\n",
    "from sklearn.metrics import confusion_matrix\n",
    "from sklearn.discriminant_analysis import LinearDiscriminantAnalysis\n",
    "from sklearn.svm import SVC\n",
    "from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis\n",
    "from sklearn.ensemble import RandomForestClassifier\n",
    "from sklearn.metrics import plot_confusion_matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.2 Apply classifiers on the data\n",
    "\n",
    "We remind that the goal of the classifiers is to `predict` a `label` choosen among a predefined list of labels. \n",
    "\n",
    "In the following sections, you will try several classifiers. The way to do it is always the same:\n",
    "\n",
    "1. Import the classifier \n",
    "2. Instanciate your classifier\n",
    "3. Fit the model with the training set\n",
    "4. Predict the result on the test set\n",
    "5. Check the quality of the result using:\n",
    "    - 5.1. The confusion matrix\n",
    "    - 5.2. The accuracy score\n",
    "6. Plot the contours\n",
    "\n",
    "Just a piece of code as example:\n",
    "```python\n",
    "from sklearn.discriminant_analysis import LinearDiscriminantAnalysis\n",
    "myModel = LinearDiscriminantAnalysis(...)\n",
    "myModel.fit(X_train, y_train)\n",
    "\n",
    "y_pred = myModel.predict(X_test)\n",
    "\n",
    "confusion_matrix(...)\n",
    "myModel.score(...)\n",
    "\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Confusion matrix \n",
    "\n",
    "The confusion matrix is a convenient way to evaluate the performances of a classifier. \n",
    "To understand the confusion matrix better, please refer to the lecture 2.\n",
    "\n",
    "##### Function to plot the confusion matrix\n",
    "\n",
    "To plot the confusion matrix in a pretty way, you will use the `plot_confusion_matrix` function from `sklearn.metrics`\n",
    "\n",
    "An example of plot of the confusion matrix is given [here](https://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 3.2.1 Linear Discriminant Analysis\n",
    "\n",
    "As a first example, we propose you to apply the `Linear Discriminant Analysis` classifier. \n",
    "\n",
    "The principle is to model the distribution of each predictive variable by a Gaussian probability law which depends on the class to be predicted and to calculate the parameters of these probability laws. Then, during the prediction, we apply Bayes' law to deduce the probability of each class knowing the values of the predictor variables. In the LDA, the boundaries between predicted classes are in fact linear. \n",
    "\n",
    "##### Questions\n",
    "\n",
    "1. Apply the recipe given above\n",
    "2. Print the scores of the fit on the train, test and full set\n",
    "3. Plot the confusion matrix. \n",
    "4. Plot the contours.\n",
    "\n",
    "\n",
    "**hints**\n",
    "\n",
    "To plot the contours and boundaries of your classifiers, we made two differents functions for you. \n",
    "\n",
    "- `plot_contours(...)` which allow you to plot contour of a single classifier\n",
    "- `plot_contours_compare(...)` which allow you to plot the contours for several classifiers\n",
    "These two functions are already developped for you and written in the associated ```libLab3_Lidar.py```\n",
    "\n",
    "- To get help on a function: `myfunction?` or `help(plot_contours)`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Example of output for LDA\n",
    "\n",
    "<img src=\"./Figures/ConfusionMatrixLDA.png\" width=\"300\">\n",
    "<img src=\"./Figures/ContoursPlotLDA.png\" width=\"300\">\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 3.2.2 Linear SVM\n",
    "\n",
    "This classifier is the one used and proposed by Brody & Lague, 2012 (reference on top of the lab.)\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 3.2.3 Decision Tree"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 3.2.4 Quadratic Discriminant Analysis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 3.2.5 Radial Basis Function kernel (RBF)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Comments\n",
    "\n",
    "No way to test all the classifiers here, the idea is just to show you how to use them.\n",
    "Feel free to explore some other classifiers. Here is the link to the page about supervised learning on the Sci-Kit Learning webpage [https://scikit-learn.org/stable/supervised_learning.html#supervised-learning](Supervised Learning). |\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4 Application to other data sets\n",
    "\n",
    "You will find other data into the repository:\n",
    "\n",
    " 2. `SnowTrees` containing a whole ski track scene with snowpack, trees and chair lift.\n",
    " 3. `Forest` containing a forest scene with bare ground and trees.\n",
    " \n",
    " If your workflow is nicely coded (usage of the external lib etc.) then it should take a few lines of code to apply it the other data sets. \n",
    " You can either directly apply your previous classification on the new data sets, or make a new training. Nonetheless, you may need some adjustement on your feature extraction (i.e., PCA). \n",
    " \n",
    "**hints** Some data come into LAS/LAZ format. This is a LiDAR standard than can be handle under Python with the `laspy` library.  We create a function in the `libLab3_Lidar` Lib, named `lazToXYZ`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#WRITE YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References\n",
    "\n",
    "This lab is based on the following article\n",
    "1. Bordu & Lague, 2012, 3D terrestrial lidar data classification of complex natural scenes using a multi-scale dimensionality criterion: Applications in geomorphology, ISPRS Journal of Photogrammetry and Remote Sensing, doi:10.1016/j.isprsjprs.2012.01.006 | https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/eds_2021-2022/blob/main/Labs/Lab_3/References/BroduLague_ISPRS.pdf"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  },
  "toc-autonumbering": false,
  "toc-showcode": false,
  "toc-showmarkdowntxt": true,
  "toc-showtags": false
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
