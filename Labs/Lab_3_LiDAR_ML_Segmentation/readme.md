# Lab_3 : LiDAR 3D cloud classification using machine learning

We aim at performing a classification over a 3D cloud obtained from ground LiDAR. The technique is based on Brodu & Lague, (2012) paper [1]. The pdf of the paper is available on this GitLab. Assuming some considerations about the 3D distribution of objects at various scales, we will compute the eigenvalues (out of PCA) for some training set. Hence we will apply some reference frame transformations, and finally train a classifier. The ultimate goal is to be able to classify a whole data set.


## Content:
--------


### Documentation:

`EDS_Lab3_Slides.pdf` :: Presentation slides

`References/BroduLague_ISPRS.pdf` :: Brodu & Lague, (2012) paper 


### Notebook and scripts:

`EDS_Lab_3_LiDARClassML.ipynb` :: Lab notebook

`libLab3_Lidar.py` :: Lab library

### Data sets directories: 

For sake of sobriety, the data are located on a `local` directory placed in `/data/EDS/Lab_3/`. 


`LiDARDunes`  ::  Dune and vegetation (**for prototpying, please use this one**)

`Forest`  :: Forest and ground

`SnowTrees`	 :: Snowpack, Trees and chair lift



