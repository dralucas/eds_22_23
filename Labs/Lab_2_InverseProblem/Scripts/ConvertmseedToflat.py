#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 11:10:11 2020

@author: alucas
"""

import glob
from obspy import read, read_inventory
from pprint import pprint
import pandas as pd
import sys
from obspy import UTCDateTime
from obspy.clients.fdsn import Client
import csv
import numpy as np
import this


workingdir   = "./dataTeil/"


listmseed    = glob.glob(workingdir+"/"+"*.mseed")

print("List of miniseed files in the directory:")
print(listmseed)





#-------------
starttime = UTCDateTime("2019-11-11T10:52:00.000000Z")
network   = "FR"
# List of stations around the Teil earthquake
ant_sta_set=("OGDF", "BANN", "OGCB", "OGCC", "SAUF", "OGCN", "RUSF")
ant_sta_set= sorted(ant_sta_set)

clientDB = "RESIF"
out_dataless_file = "station_coord.csv"
#-------------

print("\nGet dataless informations from RESIF.")
client = Client(clientDB)


print("Coordinates of the stations:\n")
for sta in ant_sta_set:
    inv = client.get_stations(network=network, station=sta) 
    print(inv[0][0]._code, inv[0][0]._latitude, inv[0][0]._longitude, inv[0][0]._elevation)

full_out_station  = workingdir+"/"+out_dataless_file

f = open(full_out_station, 'wt')

writer = csv.writer(f)
writer.writerow(("station", \
                 "latitude",\
                 "longitude", \
                 "elevation"))
for sta in ant_sta_set:
    inv = client.get_stations(network="FR", station=sta) 
    writer.writerow((inv[0][0]._code, \
                    inv[0][0]._latitude, \
                    inv[0][0]._longitude, \
                    inv[0][0]._elevation))
f.close()
print("Information saved in {}".format(full_out_station))




#----------------------------------------
channels = read(listmseed[0])                 # read the miniseed file


sta_list = []              
sta_list_pd = []                              # Pandas structure used to sort the data
channel_list = []                             # To create the list of channels
for i, cha in enumerate(channels):            # Create a list of dictionnary to sort the data
    sta_dict = {}
    sta_dict["net"] = cha.stats.network
    sta_dict["sta"] = cha.stats.station
    sta_dict["loc"] = cha.stats.location
    sta_dict["chan"] = cha.stats.channel
    sta_dict["sps"] = cha.stats.sampling_rate
    sta_dict["npts"] = cha.stats.npts
    sta_dict["idx"] = i
    sta_list_pd.append(sta_dict)
    channel_list.append(cha)

df_channels = pd.DataFrame(data=sta_list_pd) # Create a Pandas struct fron the previous list of dict   
print("Pandas structures of the data done")   
channel_list_calib = channel_list.copy()
print("***************************************")

#-----------------------------------------------
# Sort data by station hence location

pd_sample_sta = df_channels.loc[(df_channels["sta"].isin(list(ant_sta_set))) & (df_channels["npts"] != 0)]
print(pd_sample_sta.head())
print(pd_sample_sta.describe())

dnet = pd_sample_sta.groupby(['net'], sort= True)            # Sort the data by network  
for net, gnet in dnet:
    dsta = gnet.groupby(['sta'], sort= True)                 # Then sort the data by station
    for sta, gsta in dsta:
        dloc = gsta.groupby(['loc'], sort= True)             # Then sort the data by location
        for loc, gloc in dloc:
            if len(gloc)>0:
                dsps = gloc.groupby(['sps'], sort= True)     # Finally sort the data by sps value
                for sps, gsps in dsps:
                    print("sta= {}, loc={}, sps={}".format(sta,loc,sps))
                    for i in range(0,len(gsps)):   # première boucle sur les SPS
                        idx = gsps.iloc[i]["idx"]
                        print(idx)
                        print(channel_list[idx].stats.channel, \
                              channel_list[idx].stats.sampling_rate, \
                              channel_list[idx].stats.starttime,\
                              channel_list[idx].stats.npts)
                        # Generate a filename
                        filename = "{}_{}_{}_{}_{}_test.csv".format(channel_list[idx].stats.network,\
                                                            channel_list[idx].stats.station,\
                                                            channel_list[idx].stats.location,
                                                            str(channel_list[idx].stats.sampling_rate),\
                                                            str(channel_list[idx].stats.starttime))
                    filename = filename.replace(":","_")
                    fullpath = workingdir+"/"+filename
                    f = open(fullpath, 'wt')
                    print(fullpath)
                    #for i in range(0,len(gsps)):   # première boucle sur les SPS
                    idx0 = gsps.iloc[0]["idx"]
                    idx1 = gsps.iloc[1]["idx"]
                    idx2 = gsps.iloc[2]["idx"]
                    print(idx0,idx1,idx2)
                    writer = csv.writer(f)
                    # Write the header
                    writer.writerow(("time","timeUTC",\
                                     channel_list[idx0].stats.channel,\
                                     channel_list[idx1].stats.channel,\
                                     channel_list[idx2].stats.channel))
                    print(channel_list[idx0].stats.station, channel_list[idx0].stats.location)
                    print(("time","timeUTC",\
                                     channel_list[idx0].stats.channel,\
                                     channel_list[idx1].stats.channel,\
                                     channel_list[idx2].stats.channel))
                    starttime = channel_list[idx0].stats.starttime
                    npts      = channel_list[idx0].stats.npts
                    sps       = channel_list[idx0].stats.sampling_rate
                    dt        = channel_list[idx0].stats.delta
                    t         = np.arange(starttime, starttime + npts/sps, dt)
                    #
                    #
                    print("Number of points: {}".format(npts))
                    # Write each lines of data
                    for pts in range(0, npts):
                        writer.writerow((t[pts].timestamp, t[pts],\
                                         channel_list[idx0].data[pts],\
                                         channel_list[idx1].data[pts],\
                                         channel_list[idx2].data[pts]))
                    f.close()            
                    
                    
                    
                    
myfile = listcvs[0]           # I am just trying with the first file... 
                              #But with a loop, job can be done on every files
print("Check the data from file: ")
print(myfile)

with open(myfile) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    mydict = {}
    keys = []
    for row in csv_reader:
        if line_count == 0:     # Get header
            print(f'Column names are {", ".join(row)}')
            line_count += 1
            for i in range(len(row)):
                print(row[i])
                keys.append(row[i])                         # define the list of keys from hearder infos.
                mydict[row[i]]=[]                           # define a dictionnary for each key where the value
                                                            #   will be a list
            
        else:                   # Get other lines
            mydict[keys[0]].append(float(row[0]))           # times
            mydict[keys[1]].append(row[1])                  # utc date time
            mydict[keys[2]].append(float(row[2]))           # channel 1
            mydict[keys[3]].append(float(row[3]))           # channel 2
            mydict[keys[4]].append(float(row[4]))           # channel 3
            line_count += 1
    print(f'Processed {line_count} lines.')



import matplotlib.pyplot as plt
fig, ax = plt.subplots()
ax.plot(mydict["time"], mydict["HH1"], 'b-', linewidth=0.7 )
ax.plot(mydict["time"], mydict["HH2"], 'r-', linewidth=0.7 )
ax.plot(mydict["time"], mydict["HHZ"], 'g-', linewidth=0.7 )

ax.set(xlabel='time (s)', ylabel='intensity in DU',
       title='Stations')
ax.grid()



                   