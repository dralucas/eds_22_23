# Lab 2 :: Inverse problem





Download the lab_2 directory: 

`wget -c https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/eds_22_23/-/archive/main/eds_22_23-main.tar.gz -O EDS_Lab2.tgz `


Then unpacked the tarball by:


`tar xfvz EDS_Lab2.tgz`
