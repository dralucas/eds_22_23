# Lab_4


## Time series of the global temperature from CNN: **!!! use cnn2 env**

 `EDS_ClimateTemperature_solution.ipynb`  :: need some tuning to work

## Detection of volcanoes from Venus Radar image: **!!! use cnn2 env**

 `EDS_VolcanoesOnVenus_solution.ipynb`	 

 add

```
warnings.filterwarnings("ignore", category=RuntimeWarning)
warnings.filterwarnings("ignore", category=UserWarning)
```


replace

`model1_trained.history['acc'],...`

## Rock classification:
 
`Kmean_on_Rock.ipynb`	

`Lets_rock.ipynb`
